import { useState, useEffect } from 'react'
import { Navigation } from './componenten/navigation'
import { Header } from './componenten/header'
import { TRACE } from './componenten/trace_info'
import { About } from './componenten/about'
import { Partners } from './componenten/partners'
import { Team } from './componenten/team'
import { Contact } from './componenten/contact'
import { Trigger } from './componenten/trigger'
import JsonData from './data/data.json'
import SmoothScroll from 'smooth-scroll'

export const scroll = new SmoothScroll('a[href*="#"]', {
  speed: 1000,
  speedAsDuration: true,
})

const App = () => {
  const [landingPageData, setLandingPageData] = useState({})
  useEffect(() => {
    setLandingPageData(JsonData)
  }, [])

  return (
    <div>
      <Navigation />
      <Header data={landingPageData.Header} />
      <TRACE data={landingPageData.TRACE} />
      <About data={landingPageData.About} />
      <Team data={landingPageData.Team} />
      <Trigger data={landingPageData.Trigger} />
      <Partners data={landingPageData.Partners} />
      <Contact data={landingPageData.Contact} />
    </div>
  )
}

export default App
