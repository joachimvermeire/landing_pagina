export const Header = (props) => {
  return (
    <header id='header'>
      <div className='intro'>
        <div className='overlay'>
          <div className='container'>
            <div className='row'>
              <div className=' col-md-10 col-md-offset-3 intro-text'>
              <img src="nav_logo/trace_your_taste_33042_40.jpg" style={{ marginLeft: "29"}}alt="Tastefever logo"/>
              <br />
              <a 
                  href='../#contact'
                  className='btn btn-custom btn-lg page-scroll'
                >
                  More info
                </a>{''}
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  )
}
