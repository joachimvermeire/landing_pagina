export const TRACE = (props) => {
    return (
      <div id='trace_info' className='text-center'>
        <div className='container'>
          <div className='col-md-12  section-title'>
           <h2>Trace your taste?</h2>
          </div>
          <div className='row'>
            <p>For several decades mass production of wine and other food related products have become more and more the standard. Certain quality labels have been created, but this still didn’t increase the quality of the products in the end. Bio labels and a lot of others became more a fashion statement instead of proving to the end consumer that they can really trust the product that they buy. Year after year the consumer started to distrust these existing labels, both ecological and quality. 
               The consumer these days wants to have the true story behind the production process. “Tracebility” is the keyword in the whole story of Trace your Taste. The consumer wants to know where the product comes from exactly and what the whole process of production was exactly in order to decide if the product meets the standards they are looking for. This isn’t possible at this moment with the existing labels and qualifications. 
               Tastefever brings the solution with an application where the consumer track the whole production process of the process, where the producer has the possibility to explain all the different steps of the production according to the story he wants to share with the consumer. The aim of Trace your Taste is to offer honesty and clarity, to share the whole process in an honest and open way and to provide correct information to the end consumer. No rules, that can always be altered of covered, to follow. Just bringing the true story to the consumer without judgement. 
               Trace your Taste is an advanced digital platform, both web based interface as IoS and Android application, which gives in a first step the producer the opportunity to create their story in a very easy and quick way and to register all the different steps and formalities for the government or certain labels they need to legally fulfill. All of this in just a few clicks on smartphone or PC. A very easy way to digitalize the work and share your story with the world!
            </p>
          </div>
        </div>
      </div>
    )
  }