export const Team = (props) => {
    return (
      <div id='team' className='text-center'>
        <div className='container'>
          <div className='col-md-8 col-md-offset-2 section-title'>
            <h2>Team</h2>
            <p>
            Our team exists of people that have proven their competences in  each their own field, all needed to make Trace your Taste a smooth and well working application and give the service needed for our users.
            </p>
          </div>
          <div id='row'>
            {props.data
              ? props.data.map((d, i) => (
                  <div key={`${d.name}-${i}`} className='col-md-4 col-sm-6 team'>
                    <div className='midden'>
                      {' '}
                      <img src={d.img} alt='...' style={{width: "110" , height: "151"}} className='midden-img' />
                      <div className='midden-content'>
                        <h4>{d.name}</h4>
                        <p>{d.job}</p>
                      </div>
                    </div>
                  </div>
                ))
              : 'loading'}
          </div>
        </div>
      </div>
    )
  }