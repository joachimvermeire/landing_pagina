export const Trigger = (props) => {

  const goForm = () => {
    props.history.push("/form_join");
  };


    return (
      <div id='register' className='text-center'>
        <div className='container'>
          <div className='section-title'>
            <h3>Want to follow tastefever closely? </h3>
            <p>
            Do you want to bring your true story to your customers? Do you want to go international in an easy way without having stuggles to find your customers? Fill in this form to request your membership to the Trace your Taste community and receive more information! You will receive a link to also download our application for smartphone
            </p>
            <a href="https://www.traceyourtaste.com"><p>
            Trace your Taste application Version 0.0.4 is now available
            </p></a>
          </div>
          <div className='row'>
                <button  onclick= {goForm} type='submit' style={{marginRight: "20"}} className='signin'>
                join our community 
                </button>
                <button  type='submit' className='signin'>
                keep informed
                </button>
          </div>
          <br />
        </div>
      </div>
    )
  }