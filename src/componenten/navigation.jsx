export const Navigation = (props) => {
    return (
      <nav id='menu' className='navbar navbar-default navbar-fixed-top'>
        <div className='container'>
          <div className='navbar-header'>
            <button
              type='button'
              className='navbar-toggle collapsed'
              data-toggle='collapse'
              data-target='#bs-example-navbar-collapse-1'
            >
              {' '}
              <span className='sr-only'>Toggle navigation</span>{' '}
              <span className='icon-bar'></span>{' '}
              <span className='icon-bar'></span>{' '}
              <span className='icon-bar'></span>{' '}
            </button>
            <a className='navbar-brand page-scroll' href='#page-top'>
             Trace your taste
            </a>{' '}
          </div>
  
          <div
            className='collapse navbar-collapse'
            id='bs-example-navbar-collapse-1'
          >
            <ul className='nav navbar-nav navbar-right'>
              <li>
                <a href='#header' className='page-scroll'>
                  Home
                </a>
              </li>
              <li>
                <a href='#trace_info' className='page-scroll'>
                  Trace your taste
                </a>
              </li>
              <li>
                <a href='#about' className='page-scroll'>
                  About us
                </a>
              </li>
              <li>
                <a href='#team' className='page-scroll'>
                  Team
                </a>
              </li>
              <li>
                <a href='#contact' className='page-scroll'>
                  Contact
                </a>
              </li>
              <li>
                <a href='#partners' className='page-scroll'>
                  Partners
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    )
  }