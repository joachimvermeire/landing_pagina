import { useState } from 'react'
import emailjs from 'emailjs-com'

const initialState = {
  name: '',
  email: '',
  phone: '',
  message: '',
}
export const Contact = (props) => {
  const [{ name, email, message , phone}, setState] = useState(initialState)

  const handleChange = (e) => {
    const { name, value } = e.target
    setState((prevState) => ({ ...prevState, [name]: value }))
  }
  const clearState = () => setState({ ...initialState })

  const handleSubmit = (e) => {
    e.preventDefault()
    console.log(name, email, message, phone)
    emailjs
      .sendForm(
        'YOUR_SERVICE_ID', 'YOUR_TEMPLATE_ID', e.target, 'YOUR_USER_ID'
      )
      .then(
        (result) => {
          console.log(result.text)
          clearState()
        },
        (error) => {
          console.log(error.text)
        }
      )
  }

  return (
    <div>
      <div id='contact'>
        <div className='container'>
          <div className='col-md-8'>
            <div className='row'>
              <div className='section-title'>
                <h2>Keep informed</h2>
                <p>
                Do you want to keep informed about the possibilities of our application as a consumer, start to use it and want to be part of our community? Subscribe now! We’ll send you all the information! And you will receive a link to download our application
                <a href="https://www.traceyourtaste.com">
                  Trace your Taste application Version 0.0.4 is now available
                </a>
                </p>
              </div>
              <form name='sentMessage' validate onSubmit={handleSubmit}>
                <div className='row'>
                  <div className='col-md-6'>
                    <div className='form-group'>
                      <input
                        type='text'
                        id='name'
                        name='name'
                        className='form-control'
                        placeholder='Name'
                        required
                        onChange={handleChange}
                      />
                      <p className='help-block text-danger'></p>
                    </div>
                  </div>
                  <div className='col-md-6'>
                    <div className='form-group'>
                      <input
                        type='email'
                        id='email'
                        name='email'
                        className='form-control'
                        placeholder='Email'
                        required
                        onChange={handleChange}
                      />
                      <p className='help-block text-danger'></p>
                    </div>
                  </div>
                </div>
                <div className='col-md-6 phone'> 
                    <div className='form-group phone'>
                      <input
                        type='phone'
                        id='phone'
                        name='phone'
                        className='form-control'
                        placeholder='Phone'
                        required
                        onChange={handleChange}
                      />
                      <p className='help-block text-danger'></p>
                    </div>
                </div>
                <div className='form-group'>
                  <textarea
                    name='message'
                    id='message'
                    className='form-control'
                    rows='4'
                    placeholder='Message'
                    required
                    onChange={handleChange}
                  ></textarea>
                  <p className='help-block text-danger'></p>
                </div>
                <p>* The personal data is collected and processed in accordance with our <a href="https://www.tastefever.com/gdpr" color="black">GDPR</a></p>
                <div id='success'></div>
                <button  type='submit' className='send btn btn-custom btn-lg'>
                  Send Message
                </button>
              </form>
            </div>
          </div>
        </div>
        </div>
    </div>
  )}