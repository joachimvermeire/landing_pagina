
export const Partners = (props) => {

    return (
      <div id='partners'>
        <div className='container'>
          <div className='section-title text-center'>
            <h2>Our partners</h2>
          </div>
          <div className='row'>
            {props.data
              ? props.data.map((d, i) => (
                  <div key={`${d.name}-${i}`} className='col-md-4'>
                    <div className='midden'>
                      <div className='midden-img'>
                        {' '}
                        <img src={d.img} alt='' />{' '}
                      </div>
                      <div className='midden-content'>
                        <div className='midden-link'> {d.name} </div>
                         <p>{d.text}</p>
                      </div>
                    </div>
                  </div>
                ))
              : 'loading'}
          </div>
        </div>
      </div>
    )
  }